var one = new Vue({
    el:'#vue-app-one',
    data:{
       title:'vue app one',
    },
    
    methods:{
    },
    
    computed:{
        greet(){
            return'hello from app one';
        }

    }
});

var two = new Vue({
    el:'#vue-app-two',
    data:{
        title:'vue app two',
    },
    
    methods:{
        changeTitle(){
            one.title="title changed";
        }
    },
    
    computed:{
        greet(){
            return'yo dudes, this app 2';
        }
        
    }
});

two.title ="Changed from outside"